# watchghost-aggregator

## Installation

```
git clone https://gitlab.com/grap-rhone-alpes/watchghost-aggregator.git

cd watchghost-aggregator

virtualenv -p python3 env
./env/bin/pip install -r ./requirements.txt
gitaggregate -c repos.yml

cd ./src && ../env/bin/python -m watchghost

(In a browser)
http://localhost:8888

```
